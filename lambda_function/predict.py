import json
import pickle
import spacy

nlp = spacy.load("en_core_web_sm", exclude=['senter', 'ner'])
with open('pipeline.pkl', 'rb') as f:
    mlb, vectorizer, clf = pickle.load(f)

def lambda_handler(event, context):
    doc = nlp(event['body'])
    X = ' '.join([str(token.lemma_).lower() for token in doc
                if token.is_alpha and token.text.lower()])
    X = vectorizer.transform([X])
    prediction = clf.predict_proba(X)
    indices = (-prediction).argsort()[:, :5]
    converted_predictions = mlb.classes_[indices]

    return {
            "statusCode": 200,
            'headers': {
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST'},
            "body": json.dumps({"data": converted_predictions[0].tolist()}),
            "isBase64Encoded": False
        }