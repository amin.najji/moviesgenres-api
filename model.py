import pandas as pd
import sys
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.naive_bayes import ComplementNB
import pickle
import spacy

def process_synopsis(texts):
    """Lemmatize the text using spacy pipeline."""
    preproc_pipe = []
    nlp = spacy.load("en_core_web_sm", exclude=['senter', 'ner'])
    for doc in nlp.pipe(texts, batch_size=500, n_process=-1):
        text = [str(tok.lemma_).lower() for tok in doc
                if tok.is_alpha and tok.text.lower()]
        preproc_pipe.append(' '.join(text))
    return preproc_pipe

def train_model(data_csv, model_output):
    """Training of the model."""
    data = pd.read_csv(data_csv)
    X = process_synopsis(data['synopsis'])

    data["genres"] = data.apply(lambda x: x["genres"].split(" "), axis=1)
    mlb = MultiLabelBinarizer()
    y = mlb.fit_transform(data["genres"])

    vectorizer = CountVectorizer(stop_words='english', max_features=35000)
    X = vectorizer.fit_transform(X)

    clf = OneVsRestClassifier((ComplementNB()))
    clf.fit(X, y)

    with open(model_output+'/pipeline.pkl', 'wb') as f:
        pickle.dump((mlb, vectorizer, clf), f)

    return

if __name__ == "__main__":
    data_csv = sys.argv[1]
    model_output = sys.argv[2]
    train_model(data_csv, model_output)