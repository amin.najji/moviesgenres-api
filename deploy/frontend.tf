resource "local_file" "url_api" {
  content  = "const url = \"${aws_api_gateway_stage.example.invoke_url}/${aws_api_gateway_resource.predict.path_part}\";"
  filename = "frontend/url.js"
}


resource "aws_s3_bucket" "website" {
  bucket = "movies-genres-predictions"
  acl    = "private"
  website {
    index_document = "index.html"
  }
}

resource "aws_s3_bucket_object" "file" {
  for_each = setunion(fileset("frontend/", "*"), toset(["url.js"]))

  bucket       = aws_s3_bucket.website.id
  key          = each.value
  source       = "frontend/${each.value}"
  acl          = "public-read"
  content_type = lookup(local.mime_types, regex("\\.[^.]+$", each.value), null)

  depends_on = [
    local_file.url_api
  ]
}

