terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.48.0"
    }
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }

  }
  backend "s3" {
    bucket         = "moviesgenres-terraform-state-backend"
    key            = "terraform.tfstate"
    region         = "eu-west-3"
    dynamodb_table = "terraform_state"
  }
  required_version = "~> 1.0"
}