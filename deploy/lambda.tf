resource "aws_ecr_repository" "lambda_function" {
  name = "lambda_function"
}

resource "docker_registry_image" "lambda_function" {
  name = "${aws_ecr_repository.lambda_function.repository_url}:latest"

  build {
    context    = "../lambda_function"
    dockerfile = "Dockerfile"
  }
}

resource "aws_lambda_function" "lambda_function" {
  function_name = "predict"
  role          = aws_iam_role.lambda_exec.arn
  image_uri     = "${aws_ecr_repository.lambda_function.repository_url}:latest"
  package_type  = "Image"

  memory_size = 350
  timeout     = 40
  depends_on  = [docker_registry_image.lambda_function]
}

resource "aws_cloudwatch_log_group" "hello_world" {
  name = "/aws/lambda/${aws_lambda_function.lambda_function.function_name}"

  retention_in_days = 30
}

resource "aws_iam_role" "lambda_exec" {
  name = "serverless_lambda"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_policy" {
  role       = aws_iam_role.lambda_exec.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_lambda_permission" "allow_api" {
  statement_id  = "AllowAPIgatewayInvokation"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_function.function_name
  principal     = "apigateway.amazonaws.com"
}