#!/usr/bin/env bash

function log {
    local PURPLE='\033[0;35m'
    local NOCOLOR='\033[m'
    local BOLD='\033[1m'
    local NOBOLD='\033[0m'
    echo -e -n "${PURPLE}${BOLD}$1${NOBOLD}${NOCOLOR}"
}

function install_packages {
    log "Installing requirements...\\n"
    pip install -q -r requirements.txt
    log " --> Done!\\n\\n"
}

function download_dataset {
    log "Downloading datasets...\\n"
    curl --silent --fail --request GET https://radix-hiring-challenge.s3.eu-west-1.amazonaws.com/train.csv > "train.csv"
    log " --> Done!\\n\\n"
}

function train_model {
    log "Training the model...\\n"
    python model.py "train.csv" "lambda_function"
    log " --> Done!\\n\\n"
}

install_packages
download_dataset
train_model