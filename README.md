# Movies Genres Prediction
## Introduction

The goal of this project is to build a Machine Learning model to predict the top 5 genres of a movie, given its synopsis. This model is then deployed as an API using AWS services. Furthermore, the whole process from data collection to the deployment of the API is automatized via a Gitlab pipeline and Terraform.

## Model and Preprocessing

The task can be expressed as a multi-label classification problem consisting of 19 labels. Moreover, the input data is text which requires additional preprocessing.

The whole process can be described as follow:
* The text is first processed using the *spacy* library to obtain the lemma of each word in the synopsis. It is then transformed to an appropriate format for the ML model using *CountVectorizer*
* The model used to classify the synopsis is a variant of the Naive Bayes Classifier called Complement Naive Bayes. This model was chosen because it is particularly suited for imbalanced data sets. It is then fitted using the One-vs-the-rest (OvR) multiclass strategy due to the multi-label nature of the problem.
* At inference, the model is used to calculate the probability of each label, and the 5 most probable labels are returned.


## Deployment and Architecture

Once trained, the model and the preprocessing pipeline are deployed as an AWS Lambda function using a Docker image. The Lambda function is tasked to preprocess and predict the genres of incoming synopsis and is implemented as the endpoint of a REST API Gateway. This API receives the synopsis of a movie via a static website hosted on AWS S3. 

<div align="center">
<img src="https://i.ibb.co/Zz7dJnQ/Untitled-Workspace-1.png" alt="AWS Architecture"  width="450"/>
</div>
